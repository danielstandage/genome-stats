#include <string.h>
#include "GsProcess.h"

struct GsOutputFileList
{
  GsOutputFile *sequence;
  GsOutputFile *gene;
  GsOutputFile *regulatory;
  GsOutputFile *combined_exon;
  GsOutputFile *combined_intron;
  GsOutputFile *single_exon;
  GsOutputFile *single_intron;
  GsOutputFile *intron_ratio;
};

void merge_distributions(GsNucleotideDistribution *x, GsNucleotideDistribution *y)
{
  x->a += y->a;
  x->c += y->c;
  x->g += y->g;
  x->t += y->t;
  x->s += y->s;
  x->w += y->w;
  x->n += y->n;
}

void process_gene(GtFeatureNode *gene, GtHashmap *sequences, GsOutputFileList *outfiles)
{
  GtStr *seqid = gt_genome_node_get_seqid((GtGenomeNode *)gene);
  char *idstr = gt_str_get(seqid);
  GtRange range = gt_genome_node_get_range((GtGenomeNode *)gene);
  GsSequence *seq = gt_hashmap_get(sequences, idstr);
  if(seq == NULL)
  {
    fprintf(stderr, "error: could not find sequence '%s' (gene on %s[%lu, %lu])", idstr, idstr, range.start, range.end);
    exit(1);
  }
  
  // Calculate stats for the gene itself
  const char *id = gt_feature_node_get_attribute(gene, "ID");
  GsNucleotideDistribution gene_dist = seq_counts(seq, &range);
  gs_output_file_print(outfiles->gene, "%s,%lu,%lu,%lu\n", id, gene_dist.a+gene_dist.c+gene_dist.g+gene_dist.t+gene_dist.s+gene_dist.w, gene_dist.c+gene_dist.g+gene_dist.s, gene_dist.a+gene_dist.c+gene_dist.g+gene_dist.t+gene_dist.s+gene_dist.w+gene_dist.n);
  
  // Now for the gene's regulatory regions, up to 1kb up- and downstream
  GtRange left_range;
  if(range.start == 1)
  {
    left_range.start = 0;
    left_range.end = 0;
  }
  else
  {
    if(range.start < 1001)
      left_range.start = 1;
    else
      left_range.start = range.start - 1000;
    left_range.end = range.start - 1;
  }
  
  GtRange right_range;
  if(range.end == seq->length)
  {
    right_range.start = 0;
    right_range.end = 0;
  }
  else
  {
    if(range.end + 1000 > seq->length)
      right_range.end = seq->length;
    else
      right_range.end = range.end + 1000;
    right_range.start = range.end + 1;
  }
  
  GsNucleotideDistribution reg_dist = {0,0,0,0,0,0,0};
  if(left_range.start != 0)
  {
    GsNucleotideDistribution stat = seq_counts(seq, &left_range);
    merge_distributions(&reg_dist, &stat);
  }
  
  if(right_range.start != 0)
  {
    GsNucleotideDistribution stat = seq_counts(seq, &right_range);
    merge_distributions(&reg_dist, &stat);
  }
  
  if(reg_dist.a+reg_dist.c+reg_dist.g+reg_dist.t+reg_dist.s+reg_dist.w == 0)
  {
    if(reg_dist.a+reg_dist.c+reg_dist.g+reg_dist.t+reg_dist.s+reg_dist.w+reg_dist.n == 0)
    {
      fprintf(stderr, "warning: gene '%s' (%s[%lu, %lu]) has no detectable regulatory region, excluding from regulatory region analysis\n", id, idstr, range.start, range.end);
    }
    else
    {
      fprintf(stderr, "warning: regulatory region for gene '%s' (%s[%lu, %lu]) contains only ambiguous nucleotides; excluded from the analysis\n", id, idstr, range.start, range.end);
    }
  }
  else
  {
    gs_output_file_print(outfiles->regulatory, "%s,%lu,%lu,%lu\n", id, reg_dist.a+reg_dist.c+reg_dist.g+reg_dist.t+reg_dist.s+reg_dist.w, reg_dist.c+reg_dist.g+reg_dist.s, reg_dist.a+reg_dist.c+reg_dist.g+reg_dist.t+reg_dist.s+reg_dist.w+reg_dist.n);
  }
}

void process_mrna(GtFeatureNode *mrna, GtHashmap *sequences, GsOutputFileList *outfiles)
{
  GtStr *seqid = gt_genome_node_get_seqid((GtGenomeNode *)mrna);
  char *idstr = gt_str_get(seqid);
  GsSequence *seq = gt_hashmap_get(sequences, idstr);
  GtRange range = gt_genome_node_get_range((GtGenomeNode *)mrna);
  if(seq == NULL)
  {
    fprintf(stderr, "error: could not find sequence '%s' (mRNA on %s[%lu, %lu])", idstr, idstr, range.start, range.end);
    exit(1);
  }
  
  // Collect exons
  GtArray *exons = gt_array_new( sizeof(GtFeatureNode *) );
  GtFeatureNodeIterator *iter = gt_feature_node_iterator_new_direct(mrna);
  GtFeatureNode *child;
  for(child = gt_feature_node_iterator_next(iter);
      child != NULL;
      child = gt_feature_node_iterator_next(iter))
  {
    if(gt_feature_node_has_type(child, "exon"))
    {
      gt_array_add(exons, child);
    }
  }
  gt_feature_node_iterator_delete(iter);
  
  gt_array_sort(exons, (GtCompare)gs_node_compare);
  if(gt_array_size(exons) < 1)
  {
    fprintf(stderr, "error: mRNA at %s[%lu, %lu] has no exons specified\n", idstr, range.start, range.end);
    exit(1);
  }
  
  GsNucleotideDistribution comb_exon_dist = {0,0,0,0,0,0,0};
  GsNucleotideDistribution comb_intron_dist = {0,0,0,0,0,0,0};
  GsNucleotideDistribution *single_exon_dists = gt_malloc( sizeof(GsNucleotideDistribution) * gt_array_size(exons) );
  unsigned long i;
  for(i = 0; i < gt_array_size(exons); i++)
  {
    GtFeatureNode *exon = *(GtFeatureNode **)gt_array_get(exons, i);
    GtRange range = gt_genome_node_get_range((GtGenomeNode *)exon);
    GsNucleotideDistribution exon_dist = seq_counts(seq, &range);
    gs_output_file_print(outfiles->single_exon, "%lu,%lu,%lu\n", exon_dist.a+exon_dist.c+exon_dist.g+exon_dist.t+exon_dist.s+exon_dist.w, exon_dist.c+exon_dist.g+exon_dist.s, exon_dist.a+exon_dist.c+exon_dist.g+exon_dist.t+exon_dist.s+exon_dist.w+exon_dist.n);
    merge_distributions(&comb_exon_dist, &exon_dist);
    single_exon_dists[i] = exon_dist;
    
    if(i > 0)
    {
      GtFeatureNode *prevexon = *(GtFeatureNode **)gt_array_get(exons, i-1);
      GtRange prevrange = gt_genome_node_get_range((GtGenomeNode *)prevexon);
      GtRange intronrange = {prevrange.end+1, range.start-1};
      GsNucleotideDistribution intron_dist = seq_counts(seq, &intronrange);
      if(intron_dist.a+intron_dist.c+intron_dist.g+intron_dist.t+intron_dist.s+intron_dist.w == 0)
      {
        if(intron_dist.n > 0)
          fprintf(stderr, "warning: intron at %s[%lu, %lu] contains only ambiguous characters; excluded from the analysis\n", idstr, intronrange.start, intronrange.end);
      }
      else
      {
        GsNucleotideDistribution flanking_exon_dist = {0,0,0,0,0,0,0};
        merge_distributions(&flanking_exon_dist, single_exon_dists + i - 1);
        merge_distributions(&flanking_exon_dist, single_exon_dists + i);
        
        gs_output_file_print(outfiles->single_intron, "%lu,%lu,%lu,%lu,%lu,%lu\n", intron_dist.a+intron_dist.c+intron_dist.g+intron_dist.t+intron_dist.s+intron_dist.w, intron_dist.c+intron_dist.g+intron_dist.s, intron_dist.a+intron_dist.c+intron_dist.g+intron_dist.t+intron_dist.s+intron_dist.w+intron_dist.n, flanking_exon_dist.a+flanking_exon_dist.c+flanking_exon_dist.g+flanking_exon_dist.t+flanking_exon_dist.s+flanking_exon_dist.w, flanking_exon_dist.c+flanking_exon_dist.g+flanking_exon_dist.s, flanking_exon_dist.a+flanking_exon_dist.c+flanking_exon_dist.g+flanking_exon_dist.t+flanking_exon_dist.s+flanking_exon_dist.w+flanking_exon_dist.n);
        merge_distributions(&comb_intron_dist, &intron_dist);
      }
    }
  }
  gt_free(single_exon_dists);
  gt_array_delete(exons);
  
  gs_output_file_print(outfiles->combined_exon, "%lu,%lu,%lu\n", comb_exon_dist.a+comb_exon_dist.c+comb_exon_dist.g+comb_exon_dist.t+comb_exon_dist.s+comb_exon_dist.w, comb_exon_dist.c+comb_exon_dist.g+comb_exon_dist.s, comb_exon_dist.a+comb_exon_dist.c+comb_exon_dist.g+comb_exon_dist.t+comb_exon_dist.s+comb_exon_dist.w+comb_exon_dist.n);
  if(comb_intron_dist.a+comb_intron_dist.c+comb_intron_dist.g+comb_intron_dist.t+comb_intron_dist.s+comb_intron_dist.w == 0)
  {
    if(comb_intron_dist.n > 0)
      fprintf(stderr, "warning: combined intron for mRNA at %s[%lu, %lu] contains only ambiguous characters; excluded from the analysis\n", idstr, range.start, range.end);
  }
  else
  {
    gs_output_file_print(outfiles->combined_intron, "%lu,%lu,%lu\n", comb_intron_dist.a+comb_intron_dist.c+comb_intron_dist.g+comb_intron_dist.t+comb_intron_dist.s+comb_intron_dist.w, comb_intron_dist.c+comb_intron_dist.g+comb_intron_dist.s, comb_intron_dist.a+comb_intron_dist.c+comb_intron_dist.g+comb_intron_dist.t+comb_intron_dist.s+comb_intron_dist.w+comb_intron_dist.n);
  }
}

GsNucleotideDistribution seq_counts(GsSequence *seq, GtRange *seqrange)
{
  unsigned long i;
  GsNucleotideDistribution stat = {0,0,0,0,0,0,0};
  for(i = seqrange->start - 1; i < seqrange->end; i++)
  {
    if(seq->seq[i] == 'A' || seq->seq[i] == 'a')
      stat.a++;
    else if(seq->seq[i] == 'C' || seq->seq[i] == 'c')
      stat.c++;
    else if(seq->seq[i] == 'G' || seq->seq[i] == 'G')
      stat.g++;
    else if(seq->seq[i] == 'T' || seq->seq[i] == 't')
      stat.t++;
    else if(seq->seq[i] == 'S' || seq->seq[i] == 's')
      stat.s++;
    else if(seq->seq[i] == 'W' || seq->seq[i] == 'w')
      stat.w++;
    else
      stat.n++;
  }
  return stat;
}

int is_type_of_interest(const char *type)
{
  if(strcmp(type, "gene") == 0) return TYPE_GENE;
  else if(strcmp(type, "mRNA") == 0) return TYPE_MRNA;
  
  return -1;
}

int gs_node_compare(GtGenomeNode **n1, GtGenomeNode **n2)
{
  return gt_genome_node_cmp(*n1, *n2);
}
