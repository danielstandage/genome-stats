#include "genometools.h"
#include "GsOutputFile.h"

struct GsOutputFile
{
  FILE *outstream;
  char filename[GS_OUTFILE_MAX_LENGTH];
};

GsOutputFile *gs_output_file_open( const char *prefix, const char *suffix,
                                   const char *path )
{
  // Allocate memory for the outfile object
  GsOutputFile *file = gt_malloc( sizeof(GsOutputFile) );
  file->outstream = NULL;

  // Construct the filename
  char *p = file->filename;
  if(path != NULL)
    p += sprintf(p, "%s/", path);
  p += sprintf(p, "%s", prefix);
  if(suffix != NULL)
    p += sprintf(p, ".%s", suffix);

  // Open and return the output stream
  file->outstream = fopen(file->filename, "w");
  if(file->outstream == NULL)
  {
    fprintf(stderr, "[GsOutputFile] error: could not open output file '%s'\n", file->filename);
    gt_free(file);
    exit(1);
  }
  return file;
}

void gs_output_file_print(GsOutputFile *file, const char *format, ...)
{
  va_list ap;
  va_start(ap, format);
  vfprintf(file->outstream, format, ap);
  va_end(ap);
}

void gs_output_file_close(GsOutputFile *file)
{
  if(file == NULL)
    return;

  fclose(file->outstream);
  gt_free(file);
  file = NULL;
}

void gs_output_file_list_init(GsOutputFileList *list, const char *label, const char *outdir)
{
  list->sequence        = gs_output_file_open(label, "seq.dstbn.csv", outdir);
  list->gene            = gs_output_file_open(label, "gene.dstbn.csv", outdir);
  list->regulatory      = gs_output_file_open(label, "reg.dstbn.csv", outdir);
  list->combined_exon   = gs_output_file_open(label, "comb.exon.dstbn.csv", outdir);
  list->combined_intron = gs_output_file_open(label, "comb.intron.dstbn.csv", outdir);
  list->single_exon     = gs_output_file_open(label, "sngl.exon.dstbn.csv", outdir);
  list->single_intron   = gs_output_file_open(label, "sngl.intron.dstbn.csv", outdir);
}

void gs_output_file_list_terminate(GsOutputFileList *list)
{
  gs_output_file_close(list->sequence);
  gs_output_file_close(list->gene);
  gs_output_file_close(list->regulatory);
  gs_output_file_close(list->combined_exon);
  gs_output_file_close(list->combined_intron);
  gs_output_file_close(list->single_exon);
  gs_output_file_close(list->single_intron);
}
