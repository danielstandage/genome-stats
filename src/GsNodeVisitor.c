#include "GsNodeVisitor.h"
#include "GsOutputFile.h"
#include "GsProcess.h"

#define gs_node_visitor_cast(GV)\
        gt_node_visitor_cast(gs_node_visitor_class(), GV)

//----------------------------------------------------------------------------//
// Data structure definitions
//----------------------------------------------------------------------------//
struct GsNodeVisitor
{
  const GtNodeVisitor parent_instance;
  GtHashmap *sequences;
  GsOutputFileList *outfiles;
};
struct GsOutputFileList
{
  GsOutputFile *sequence;
  GsOutputFile *gene;
  GsOutputFile *regulatory;
  GsOutputFile *combined_exon;
  GsOutputFile *combined_intron;
  GsOutputFile *single_exon;
  GsOutputFile *single_intron;
};


//----------------------------------------------------------------------------//
// Prototypes of private functions
//----------------------------------------------------------------------------//

/**
 * Cast a node visitor object as a GsNodeVisitor
 *
 * @returns    a node visitor object cast as a GsNodeVisitor
 */
const GtNodeVisitorClass* gs_node_visitor_class();

/**
 * Destructor for the GsNodeVisitor class
 *
 * @param[in] nv    the node visitor object
 */
static void gs_node_visitor_free(GtNodeVisitor *nv);

/**
 * Delete any comment nodes encountered while loading the data
 *
 * @param[in]  nv       a node visitor
 * @param[in]  cn       node representing a GFF3 comment entry
 * @param[out] error    error object to which error messages, if any, will be written
 * @returns             0 for success
 */
static int gs_node_visitor_visit_comment_node(GtNodeVisitor *nv, GtCommentNode *cn, GT_UNUSED GtError *error);

/**
 * Delete any EOF nodes encountered while loading the data
 *
 * @param[in]  nv       a node visitor
 * @param[in]  en       node representing the end of a GFF3 file
 * @param[out] error    error object to which error messages, if any, will be written
 * @returns             0 for success
 */
static int gs_node_visitor_visit_eof_node(GtNodeVisitor *nv, GtEOFNode *en, GT_UNUSED GtError *error);

/**
 * Validate and store any feature nodes encountered while loading the data
 *
 * @param[in]  nv       a node visitor
 * @param[in]  fn       node representing a GFF3 feature entry
 * @param[out] error    error object to which error messages, if any, will be written
 * @returns             0 for success
 */
static int gs_node_visitor_visit_feature_node(GtNodeVisitor *nv, GtFeatureNode *fn, GT_UNUSED GtError *error);

/**
 * Delete any region nodes encountered while loading the data
 *
 * @param[in]  nv       a node visitor
 * @param[in]  rn       node representing a GFF3 sequence-region entry
 * @param[out] error    error object to which error messages, if any, will be written
 * @returns             0 for success
 */
static int gs_node_visitor_visit_region_node(GtNodeVisitor *nv, GtRegionNode *rn, GT_UNUSED GtError *error);

/**
 * Delete any sequence nodes encountered while loading the data
 * At some point these will be handled differently, but for now this is it
 *
 * @param[in]  nv       a node visitor
 * @param[in]  sn       node representing a Fasta sequence
 * @param[out] error    error object to which error messages, if any, will be written
 * @returns             0 for success
 */
static int gs_node_visitor_visit_sequence_node(GtNodeVisitor *nv, GtSequenceNode *sn, GT_UNUSED GtError *error);


//------------------------------------------------------------------------------------------------//
// Method implementations
//------------------------------------------------------------------------------------------------//
const GtNodeVisitorClass* gs_node_visitor_class()
{
  static const GtNodeVisitorClass *nvc = NULL;
  if(!nvc)
  {
    nvc = gt_node_visitor_class_new( sizeof (GsNodeVisitor),
                                     gs_node_visitor_free,
                                     gs_node_visitor_visit_comment_node,
                                     gs_node_visitor_visit_feature_node,
                                     gs_node_visitor_visit_region_node,
                                     gs_node_visitor_visit_sequence_node,
                                     gs_node_visitor_visit_eof_node );
  }
  return nvc;
}

static void gs_node_visitor_free(GtNodeVisitor *nv)
{
  GT_UNUSED GsNodeVisitor *aiv = gs_node_visitor_cast(nv);
}

GtNodeVisitor *gs_node_visitor_new(GtHashmap *sequences, GsOutputFileList *outfiles)
{
  GtNodeVisitor *nv;
  nv = gt_node_visitor_create(gs_node_visitor_class());
  GsNodeVisitor *v = gs_node_visitor_cast(nv);

  v->sequences = sequences;
  v->outfiles = outfiles;

  return nv;
}

static int gs_node_visitor_visit_comment_node(GtNodeVisitor *nv, GtCommentNode *cn, GT_UNUSED GtError *error)
{
  gt_error_check(error);
  gt_genome_node_delete((GtGenomeNode *)cn);
  return 0;
}

static int gs_node_visitor_visit_eof_node(GtNodeVisitor *nv, GtEOFNode *en, GT_UNUSED GtError *error)
{
  gt_error_check(error);
  gt_genome_node_delete((GtGenomeNode *)en);
  return 0;
}

static int gs_node_visitor_visit_feature_node(GtNodeVisitor *nv, GtFeatureNode *fn, GT_UNUSED GtError *error)
{
  GsNodeVisitor *v;
  gt_error_check(error);
  v = gs_node_visitor_cast(nv);
  
  GtFeatureNodeIterator *iter = gt_feature_node_iterator_new(fn);
  GtFeatureNode *current;
  for(current = gt_feature_node_iterator_next(iter);
      current != NULL;
      current = gt_feature_node_iterator_next(iter))
  {
    const char *typestr = gt_feature_node_get_type(current);
    int type = is_type_of_interest(typestr);
    switch(type)
    {
      case TYPE_GENE:
        process_gene(current, v->sequences, v->outfiles);
        /*gs_output_file_print(v->outfiles->gene, "%s,%lu,%lu,%lu\n", id, s.a+s.c+s.g+s.t+s.s+s.w, s.c+s.g+s.s, s.a+s.c+s.g+s.t+s.s+s.w+s.n);
        
        s = process_regulatory(current, v->sequences);
        gs_output_file_print(v->outfiles->regulatory, "%s,%lu,%lu,%lu\n", id, s.a+s.c+s.g+s.t+s.s+s.w, s.c+s.g+s.s, s.a+s.c+s.g+s.t+s.s+s.w+s.n);*/
        break;
      
      case TYPE_MRNA:
        process_mrna(current, v->sequences, v->outfiles);
        /*gs_output_file_print(v->outfiles->combined_exon, "%lu,%lu,%lu\n", s.a+s.c+s.g+s.t+s.s+s.w, s.c+s.g+s.s, s.a+s.c+s.g+s.t+s.s+s.w+s.n);
        
        s = process_intron_aggr(current, v->sequences);
        if(s.a+s.c+s.g+s.t+s.s+s.w != 0)
          gs_output_file_print(v->outfiles->combined_intron, "%lu,%lu,%lu\n", s.a+s.c+s.g+s.t+s.s+s.w, s.c+s.g+s.s, s.a+s.c+s.g+s.t+s.s+s.w+s.n);*/
        break;
      
      /*case TYPE_EXON:
        s = process_exon(current, v->sequences);
        gs_output_file_print(v->outfiles->single_exon, "%lu,%lu,%lu\n", s.a+s.c+s.g+s.t+s.s+s.w, s.c+s.g+s.s, s.a+s.c+s.g+s.t+s.s+s.w+s.n);
        break;
      
      case TYPE_INTRON:
        s = process_intron(current, v->sequences);
        if(s.a+s.c+s.g+s.t+s.s+s.w != 0)
          gs_output_file_print(v->outfiles->single_intron, "%lu,%lu,%lu\n", s.a+s.c+s.g+s.t+s.s+s.w, s.c+s.g+s.s, s.a+s.c+s.g+s.t+s.s+s.w+s.n);
        break;*/
      
      default:
        break;
    }
  }
  gt_feature_node_iterator_delete(iter);
  gt_genome_node_delete((GtGenomeNode *)fn);

  return 0;
}

static int gs_node_visitor_visit_region_node(GtNodeVisitor *nv, GtRegionNode *rn, GT_UNUSED GtError *error)
{
  gt_error_check(error);
  gt_genome_node_delete((GtGenomeNode *)rn);
  return 0;
}

static int gs_node_visitor_visit_sequence_node(GtNodeVisitor *nv, GtSequenceNode *sn, GT_UNUSED GtError *error)
{
  gt_error_check(error);
  gt_genome_node_delete((GtGenomeNode *)sn);
  return 0;
}