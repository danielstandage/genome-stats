#include <zlib.h>
#include "genometools.h"
#include "GsSequence.h"
#include "kseq.h"
KSEQ_INIT(gzFile, gzread)

GsSequence *gs_sequence_new(const char *seq, const char *name)
{
  GsSequence *sequence = gt_malloc( sizeof(GsSequence) );
  sequence->seq = gt_cstr_dup(seq);
  sequence->name = gt_cstr_dup(name);
  sequence->length = strlen(seq);
  return sequence;
}

void gs_sequence_delete(GsSequence *seq)
{
  if(seq == NULL)
    return;
  gt_free(seq->seq);
  gt_free(seq->name);
  gt_free(seq);
  seq = NULL;
}

struct GsSequenceFile
{
  gzFile fasta;
  kseq_t *seq;
};

GsSequenceFile *gs_sequence_file_open(const char *filename)
{
  GsSequenceFile *file = gt_malloc( sizeof(GsSequenceFile) );
  file->fasta = gzopen(filename, "r");
  file->seq = kseq_init(file->fasta);
  return file;
}

GsSequence *gs_sequence_file_next(GsSequenceFile *seqfile)
{
  int code = kseq_read(seqfile->seq);
  if(code < 0)
    return NULL;
  
  GsSequence *seq = gs_sequence_new(seqfile->seq->seq.s, seqfile->seq->name.s);
  return seq;
}

void gs_sequence_file_close(GsSequenceFile *seqfile)
{
  if(seqfile == NULL)
    return;
  kseq_destroy(seqfile->seq);
  gzclose(seqfile->fasta);
  gt_free(seqfile);
  seqfile = NULL;
}
