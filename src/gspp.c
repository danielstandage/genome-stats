#include <getopt.h>
#include <string.h>
#include "genometools.h"
#include "GsNodeVisitor.h"
#include "GsOutputFile.h"
#include "GsProcess.h"
#include "GsSequence.h"

void print_usage(FILE *outstream)
{
  fprintf( outstream,
           "Usage: gspp [options] genome.fasta annotation.gff3\n"
           "  Options:\n"
           "    -h|--help      print this help message and exit\n"
           "    -l|--label     the name of this job, which will serve as the prefix of all\n"
           "                   output files generated; default is 'genome'\n"
           "    -o|--outdir    the directory to which output files will be written; default\n"
           "                   is current directory\n"
           "    -t|--term      report GC-content for this many nucleotides at each terminus\n"
           "                   of each sequence; default is 1000\n" );
}

int main(int argc, char **argv)
{
  const char *fasta_file;
  const char *gff3_file;
  const char *label = "genome";
  const char *outdir = ".";
  unsigned long termlen = 1000;

  // Parse options
  int opt = 0;
  int optindex = 0;
  const char *optstr = "hl:o:";
  const struct option gspp_options[] =
  {
    { "help", no_argument, NULL, 'h' },
    { "label", required_argument, NULL, 'l' },
    { "outdir", required_argument, NULL, 'o' },
    { "term", required_argument, NULL, 't' },
    { NULL, no_argument, NULL, 0 },
  };

  for( opt = getopt_long(argc, argv, optstr, gspp_options, &optindex);
       opt != -1;
       opt = getopt_long(argc, argv, optstr, gspp_options, &optindex) )
  {
    switch(opt)
    {
      case 'h':
        print_usage(stdout);
        return 0;
        break;

      case 'l':
        label = optarg;
        break;

      case 'o':
        outdir = optarg;
        break;

      case 't':
        termlen = atol(optarg);
        break;

      default:
        break;
    }
  }

  int filenum = argc - optind;
  if(filenum != 2)
  {
    fprintf(stderr, "error: please provide 2 input files (sequence file (Fasta format) and annotation file (GFF3 format))\n");
    print_usage(stderr);
    return 1;
  }
  fasta_file = argv[optind + 0];
  gff3_file  = argv[optind + 1];

  // Initialize GenomeTools library, initialize output files
  gt_lib_init();
  GsOutputFileList outfiles;
  gs_output_file_list_init(&outfiles, label, outdir);

  // Load sequences into memory
  GtHashmap *sequences = gt_hashmap_new(GT_HASH_STRING, NULL, (GtFree)gs_sequence_delete);
  GsSequenceFile *seqfile = gs_sequence_file_open(fasta_file);
  GsSequence *seq;
  while((seq = gs_sequence_file_next(seqfile)) != NULL)
  {
    GsSequence *test = gt_hashmap_get(sequences, seq->name);
    if(test != NULL)
    {
      fprintf(stderr, "warning: sequence '%s' is not unique; overwriting now\n", seq->name);
    }
    gt_hashmap_add(sequences, seq->name, seq);

    GtRange seqrange = {1, seq->length};
    GsNucleotideDistribution s = seq_counts(seq, &seqrange);

    GtRange fiveprimerange = {1, termlen};
    GtRange threeprimerange = {seq->length - termlen + 1, seq->length};
    GsNucleotideDistribution x = {0,0,0,0,0,0,0};
    GsNucleotideDistribution y = {0,0,0,0,0,0,0};
    if(termlen >= seq->length)
    {
      merge_distributions(&x, &s);
      merge_distributions(&y, &s);
    }
    else
    {
     GsNucleotideDistribution xtemp = seq_counts(seq, &fiveprimerange);
     merge_distributions(&x, &xtemp);
     GsNucleotideDistribution ytemp = seq_counts(seq, &threeprimerange);
     merge_distributions(&y, &ytemp);
    }

    gs_output_file_print(outfiles.sequence, "%s,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu\n", seq->name, s.a+s.c+s.g+s.t+s.s+s.w, s.c+s.g+s.s, seq->length, x.a+x.c+x.g+x.t+x.s+x.w, x.c+x.g+x.s, x.a+x.c+x.g+x.t+x.s+x.w+x.n, y.a+y.c+y.g+y.t+y.s+y.w, y.c+y.g+y.s, y.a+y.c+y.g+y.t+y.s+y.w+y.n);
  }
  gs_sequence_file_close(seqfile);

  // Load annotations into memory
  GtNodeStream *gff3 = gt_gff3_in_stream_new_unsorted(1, &gff3_file);
  gt_gff3_in_stream_check_id_attributes((GtGFF3InStream *)gff3);
  gt_gff3_in_stream_enable_tidy_mode((GtGFF3InStream *)gff3);
  GtError *error = gt_error_new();
  GtNodeVisitor *nv = gs_node_visitor_new(sequences, &outfiles);
  GtGenomeNode *gn;
  int had_error = 0;
  while(!(had_error = gt_node_stream_next(gff3, &gn, error)) && gn)
  {
    gt_genome_node_accept(gn, nv, error);
  }
  gs_output_file_list_terminate(&outfiles);
  gt_node_stream_delete(gff3);
  gt_node_visitor_delete(nv);
  gt_hashmap_delete(sequences);

  if(had_error)
  {
    fprintf(stderr, "error: '%s'\n", gt_error_get(error));
  }
  gt_error_delete(error);

  gt_lib_clean();
  return had_error ? EXIT_FAILURE : 0;
}
