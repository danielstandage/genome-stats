GT_INSTALL_DIR=/usr/local

CC=gcc
CFLAGS=-Wall -O3 -DWITHOUT_CAIRO
ifeq ($(64bit),yes)
  CFLAGS += -m64
endif

bin/gspp:		src/gspp.c obj/GsOutputFile.o obj/GsSequence.o obj/GsNodeVisitor.o obj/GsProcess.o
			@- test -d bin || mkdir bin
			$(CC) $(CFLAGS) -I $(GT_INSTALL_DIR)/include/genometools -I inc -o bin/gspp src/gspp.c obj/GsOutputFile.o obj/GsSequence.o obj/GsNodeVisitor.o obj/GsProcess.o -lz -lgenometools

clean:			
			rm -f bin/gspp obj/* 

obj/GsOutputFile.o:	src/GsOutputFile.c inc/GsOutputFile.h
			@- test -d obj || mkdir obj
			$(CC) -c $(CFLAGS) -I $(GT_INSTALL_DIR)/include/genometools -I inc -o obj/GsOutputFile.o src/GsOutputFile.c

obj/GsSequence.o:	src/GsSequence.c inc/GsSequence.h
			@- test -d obj || mkdir obj
			$(CC) -c $(CFLAGS) -I $(GT_INSTALL_DIR)/include/genometools -I inc -o obj/GsSequence.o src/GsSequence.c

obj/GsNodeVisitor.o:	src/GsNodeVisitor.c inc/GsNodeVisitor.h
			@- test -d obj || mkdir obj
			$(CC) -c $(CFLAGS) -I $(GT_INSTALL_DIR)/include/genometools -I inc -o obj/GsNodeVisitor.o src/GsNodeVisitor.c

obj/GsProcess.o:	src/GsProcess.c inc/GsProcess.h
			@- test -d obj || mkdir obj
			$(CC) -c $(CFLAGS) -I $(GT_INSTALL_DIR)/include/genometools -I inc -o obj/GsProcess.o src/GsProcess.c
