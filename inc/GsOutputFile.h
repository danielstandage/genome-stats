#ifndef GENOME_STATS_OUTPUT_FILE
#define GENOME_STATS_OUTPUT_FILE

#define GS_OUTFILE_MAX_LENGTH 1024

/**
 * A simple data structure intended to facilitate the opening and closing of
 * file output streams by aggregating the associated filename strings and stream
 * objects.
 */
typedef struct GsOutputFile GsOutputFile;

/**
 * Construct the output filename of from the given prefix, suffix, and path, and
 * open the output stream.
 *
 * @param[in] prefix    the prefix of the filename, typically the 'label' option
 *                      specified by the main program
 * @param[in] suffix    the suffix of the filename, which will be separated from
 *                      the prefix with a period; if set to NULL, no suffix will
 *                      be used
 * @param[in] path      the path of the directory to which the file will be
 *                      written; if set to NULL, file will be written to current
 *                      directory
 * @returns             a GsOutputFile object
 */
GsOutputFile *gs_output_file_open( const char *prefix, const char *suffix,
                                   const char *path );

/**
 * A printf-style function for printing to the given output file.
 *
 * @param[in] file      pointer to the output file
 * @param[in] format    printf-style formatted string
 * @param[in] ...       variable argument list for format
 */
void gs_output_file_print(GsOutputFile *file, const char *format, ...);

/**
 * Close the output file and release the memory previously associated witht this
 * object.
 *
 * @param[in] file    the file to close and free
 */
void gs_output_file_close(GsOutputFile *file);

/**
 *
 */
typedef struct
{
  GsOutputFile *sequence;
  GsOutputFile *gene;
  GsOutputFile *regulatory;
  GsOutputFile *combined_exon;
  GsOutputFile *combined_intron;
  GsOutputFile *single_exon;
  GsOutputFile *single_intron;
} GsOutputFileList;

/**
 *
 */
void gs_output_file_list_init(GsOutputFileList *list, const char *label, const char *outdir);

/**
 *
 */
void gs_output_file_list_terminate(GsOutputFileList *list);

#endif
