#ifndef GENOME_STATS_PROCESS
#define GENOME_STATS_PROCESS

#include "genometools.h"
#include "GsOutputFile.h"
#include "GsSequence.h"

#define TYPE_GENE   1
#define TYPE_MRNA   2

typedef struct
{
  unsigned long a;
  unsigned long c;
  unsigned long g;
  unsigned long t;
  unsigned long s; // 'strong' bases, C or G
  unsigned long w; // 'weak' bases, A or T
  unsigned long n; // N or any other ambiguous IUPAC bases besides S and W
} GsNucleotideDistribution;

void merge_distributions(GsNucleotideDistribution *x, GsNucleotideDistribution *y);

void process_gene(GtFeatureNode *gene, GtHashmap *sequences, GsOutputFileList *outfiles);

void process_mrna(GtFeatureNode *mrna, GtHashmap *sequences, GsOutputFileList *outfiles);

GsNucleotideDistribution seq_counts(GsSequence *seq, GtRange *seqrange);

int is_type_of_interest(const char *type);

int gs_node_compare(GtGenomeNode **n1, GtGenomeNode **n2);

#endif
