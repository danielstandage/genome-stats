#ifndef GENOME_STATS_SEQUENCE
#define GENOME_STATS_SEQUENCE

/**
 * Simple data structure for aggregating a nucleotide sequence with its
 * corresponding name and length.
 */
typedef struct
{
  char *seq;
  char *name;
  unsigned long length;
} GsSequence;

/**
 * Create a new sequence object, making copies of the sequence and name strings.
 *
 * @param[in] seq     a genomic nucleotide sequence
 * @param[in] name    the unique name or ID for this sequence
 * @returns           a sequence object
 */
GsSequence *gs_sequence_new(const char *seq, const char *name);

/**
 * Free the memory occupied by this sequence object.
 *
 * @param[in] seq    a sequence object
 */
void gs_sequence_delete(GsSequence *seq);

/**
 * A data structure facilitating parsing sequences from a Fasta file.
 */
typedef struct GsSequenceFile GsSequenceFile;

/**
 * Open a sequence stream.
 *
 * @param[in] filename    the name of the sequence file
 * @returns               an input file stream
 */
GsSequenceFile *gs_sequence_file_open(const char *filename);

/**
 * Grab the next sequence from the given input file.
 *
 * @param[in] seqfile    input file stream
 * @returns              a sequence object, or NULL if the end of the file has
 *                       been reached
 */
GsSequence *gs_sequence_file_next(GsSequenceFile *seqfile);

/**
 * Close the input file stream and free the memory occupied by the object.
 *
 * @param[in] seqfile    input file stream
 */
void gs_sequence_file_close(GsSequenceFile *seqfile);

#endif
