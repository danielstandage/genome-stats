#ifndef GENOME_STATS_NODE_VISITOR
#define GENOME_STATS_NODE_VISITOR

#include "genometools.h"
#include "GsOutputFile.h"

typedef struct GsNodeVisitor GsNodeVisitor;

/**
 * Genome node visitor for processing feature nodes from GFF3 input.
 *
 * @param[in] sequences    a collection of sequence objects
 * @param[in] outfiles     a list of output files
 * @returns                a node visitor object
 */
GtNodeVisitor *gs_node_visitor_new(GtHashmap *sequences, GsOutputFileList *outfiles);

#endif
