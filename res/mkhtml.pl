#!/usr/bin/env perl
use strict;
use Getopt::Long;

sub print_usage
{
  my $outstream = shift(@_);
  printf($outstream "Usage: mkhtml.pl [options] < template.html > index.html
  Options:
    --data:     data file produced by stats.r
    --help:     print this help message and exit
    --label:    name of the job
");
}

my $datafile = "genome.stats.txt";
my $jobname = "genome";
GetOptions
(
  "data=s"  => \$datafile,
  "help"    => sub{ print_usage(\*STDOUT); exit(0); },
  "label=s" => \$jobname,
);


open(my $DAT, "<", $datafile) or die("error: unable to open data file '$datafile'");
my $data = {};

while(my $line = <$DAT>)
{
  if($line =~ m/BEGIN: +(\S+)/)
  {
    my $type = $1;
    $data->{$type} = {};
    while($line = <$DAT>)
    {
      last if($line =~ m/^END/);
      
      chomp($line);
      my($key, $value) = $line =~ m/^(\S+): +(.+)$/;
      
      if($key eq "comblen" and $type ne "seq")
      {
        $data->{$type}->{"lenperc"} = sprintf("%.2f", $value / $data->{"seq"}->{"comblen"} * 100);
      }
      elsif($key eq "lendist")
      {
        $value =~ s/([,\]])/ bp$1/g;
      }
      elsif($key eq "gcagg" or $key eq "gcavg")
      {
        $value = sprintf("%.2f", $value * 100);
      }
      elsif($key eq "gcdist")
      {
        my($valstr) = $value =~ m/\[(.+)\]/;
        my @values = split(/\s*,\s*/, $valstr);
        @values = map { sprintf("%.2f%%", $_ * 100) } @values;
        $value = sprintf("[%s]", join(", ", @values));
      }
      elsif($key eq "lm")
      {
        my @values = split(/\s*,\s*/, $value);
        $data->{$type}->{"r2"} = $values[0];
        $data->{$type}->{"r2adj"} = $values[1];
        $data->{$type}->{"p"} = $values[2];
        $data->{$type}->{"a"} = $values[3];
        $data->{$type}->{"b"} = $values[4];
      }
      elsif($key eq "ratdist")
      {
        my($valstr) = $value =~ m/\[(.+)\]/;
        my @values = split(/\s*,\s*/, $valstr);
        @values = map { sprintf("%.4f", $_) } @values;
        $value = sprintf("[%s]", join(", ", @values));
      }
      
      $data->{$type}->{$key} = $value;
    }
  }
}

my $html;
{
  local $/;
  $html = <STDIN>;
  $html =~ s/\$job/$jobname/g;
  
  foreach my $type(keys(%$data))
  {
    foreach my $key(keys(%{$data->{$type}}))
    {
      my $search = sprintf("\\\$%s\_%s", $type, $key);
      my $replace = $data->{$type}->{$key};
      $html =~ s/$search/$replace/g;
    }
  }
}

print($html);
